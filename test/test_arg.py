#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
explain here what is tested
"""

from __future__ import print_function
from __future__ import division
from __future__ import absolute_import

import unittest

class TestExample(unittest.TestCase):
    """example of test class"""

    def test_func_01(self):
        """test case 01"""

        value_in = 1
        value_out = 1

        self.assertEqual(value_in, value_out)


if __name__ == '__main__':
    unittest.main()