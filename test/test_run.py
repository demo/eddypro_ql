#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division
from __future__ import absolute_import

import subprocess
import os
import glob
import shutil

import pytest


MAIN_DIR = os.path.dirname(os.path.dirname(__file__))
TEST_DIR = os.path.join(MAIN_DIR, 'test')
TEST_IN_DIR = os.path.join(TEST_DIR, 'data')
TEST_OUT_DIR = os.path.join(TEST_DIR, 'output')
TMP_DIR = os.path.join(TEST_DIR, 'tmp')
PRGM = "eddypro_ql_aeris.py"


@pytest.mark.parametrize('date, file_', [
    (
        '20170814',
        'P2OA_EP-Eddy-Covariance_L2A-10min-Mean-30m-Alt_2017-08-14T00-00-00_1D_V1-00.nc',
    ),
    (
        '20170814',
        'P2OA_EP-Eddy-Covariance_L2A-10min-Mean-45m-Alt_2017-08-14T00-00-00_1D_V1-00.nc',
    ),
    (
        '20170814',
        'P2OA_EP-Eddy-Covariance_L2A-10min-Mean-60m-Alt_2017-08-14T00-00-00_1D_V1-00.nc',
    ),
    (
        '20170814',
        'P2OA_EP-Eddy-Covariance_L2A-30min-Mean-30m-Alt_2017-08-14T00-00-00_1D_V1-00.nc',
    ),
    (
        '20170814',
        'P2OA_EP-Eddy-Covariance_L2A-30min-Mean-45m-Alt_2017-08-14T00-00-00_1D_V1-00.nc',
    ),
    (
        '20170814',
        'P2OA_EP-Eddy-Covariance_L2A-30min-Mean-60m-Alt_2017-08-14T00-00-00_1D_V1-00.nc',
    ),
    (
        '20171018',
        'SIRTA_EP-Eddy-Covariance_L2A-10min-Mean-2m-Alt_2017-10-18T00-00-00_1D_V1-00.nc',
    ),
    (
        '20171018',
        'SIRTA_EP-Eddy-Covariance_L2A-10min-Mean-30m-Alt_2017-10-18T00-00-00_1D_V1-00.nc',
    ),
    (
        '20171018',
        'SIRTA_EP-Eddy-Covariance_L2A-30min-Mean-2m-Alt_2017-10-18T00-00-00_1D_V1-00.nc',
    ),
    (
        '20171215',
        'CNRM_EP-Eddy-Covariance_L2A-30min-Mean-3.5m-Alt_2017-12-15T00-00-00_1D_V1-00.nc',
    ),

])
def test_run(date, file_):
    """Run program on selected data files to check quicklooks."""
    file_in = os.path.join(TEST_IN_DIR, file_)

    resp = subprocess.call([
        os.path.join(MAIN_DIR, PRGM),
        date,
        file_in,
        TMP_DIR
    ])

    # rename output files
    # ------------------------------------------------------------------------
    # get name of the input file without extension
    basename = os.path.basename(file_in)
    prefix = os.path.splitext(basename)[0]

    list_img = glob.glob(os.path.join(TMP_DIR, '*.png'))
    for file_ in list_img:
        # get one character id of the file
        out_file_ind = os.path.basename(file_).split('.')[0].split('_')[-1]
        # new name of the image
        new_name = '{}_{}.png'.format(prefix, out_file_ind)
        # move from tmp dir to output
        shutil.move(file_, os.path.join(TEST_OUT_DIR, new_name))

    assert resp == 0
