"""Tools for tests suite."""


def create_list_files(list_files: list, filename: str):
    """Create input files using SIRTA data workflow format."""
    with open(filename, 'w') as fid:
        for file_ in list_files:
            fid.write(f'{file_}\n')
