#!/usr/bin/env python
# -*- coding : utf8 -*-

"""
Code to plot AERIS eddyPro netCDF files.

Copyright (c) CNRS/ecole polytechnique
"""

from __future__ import print_function
from __future__ import division
from __future__ import absolute_import

import sys
import os
import argparse
import datetime as dt

import numpy as np
import netCDF4 as nc
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.dates as mdates
import matplotlib.patches as mpatches

from utils import utils
from utils import args_parser as ag
from utils import plot

__version__ = '1.1.1'
__author__ = 'Marc-Antoine Drouin'

# script description
# ----------------------------------------------------------------------------
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
PROG_NAME = 'eddypro_ql_aeris'
PROG_DESCR = 'flux to create eddypro QL for AERIS processing'

# physical parameters
# ----------------------------------------------------------------------------
DEG_2_K = 273.15
ALMOST_ONE_DAY = dt.timedelta(hours=23, minutes=59, seconds=59)
SECONDS_PER_DAY = 86400

# variables to read in netCDF file
# ----------------------------------------------------------------------------
SCALAR_VARS = [
    'sensor_lat',
    'sensor_lon',
    'sensor_alt',
    'sensor_alt_agl',
    'station_lat',
    'station_lon',
    'station_alt',
    'station_name',
    'instrument_type',
]

TIME_VARS = [
    'time',
]

VECTOR_VARS = [
    'ws', 'wd',
    'air_temp', 'virt_temp', 'dew_temp',
    'ts_spikes',
    'h2o_mixing_ratio', 'co2_mixing_ratio',
    'h2o_molar_density', 'co2_molar_density',
    'h2o_spikes', 'co2_spikes',
    'H', 'H_qc', 'LE', 'LE_qc', 'co2_flux', 'co2_flux_qc',
    'u_star', 'z_minus_d_over_L',
    'TKE',
    'bowen_ratio',
    'u', 'v', 'w',
    'u_spikes', 'v_spikes', 'w_spikes',
    'u_var', 'v_var', 'w_var',
    'sonic_air_temp_var',
    'tau', 'tau_qc',
    'used_record',
    'h2o_timelag', 'co2_timelag',
]

# plot extrema configuration
# ----------------------------------------------------------------------------

# Values to correct y_lim in case of some values are above extrema
# if data are above min_lim or max_lim extrema of axis are set
# to min_axis and max_axis
PLOT_THRESHOLD = {
    'ws': {
        'min_axis': 0.,
        'max_axis': 30.,
        'min_lim': -1.,
        'max_lim': 100,
    },
    'wd': {
        'min_axis': 0.,
        'max_axis': 360.,
        'min_lim': -1.,
        'max_lim': 361.,
    },
    'air_temp': {
        'min_axis': -10.,
        'max_axis': 40.,
        'min_lim': -100.,
        'max_lim': 100.,
    },
    'virt_temp': {
        'min_axis': -10.,
        'max_axis': 40.,
        'min_lim': -100.,
        'max_lim': 100.,
    },
    'dew_temp': {
        'min_axis': -10.,
        'max_axis': 40.,
        'min_lim': -100.,
        'max_lim': 100.,
    },
    'h2o_mixing_ratio': {
        'min_axis': 0.,
        'max_axis': 40.,
        'min_lim': -1.,
        'max_lim': 100.,
    },
    'co2_mixing_ratio': {
        'min_axis': 200.,
        'max_axis': 900.,
        'min_lim': 0.,
        'max_lim': 1500.,
    },
    'h2o_molar_density': {
        'min_axis': 0.,
        'max_axis': 1600.,
        'min_lim': -1.,
        'max_lim': 3000.,
    },
    'co2_molar_density': {
        'min_axis': 8.,
        'max_axis': 36.,
        'min_lim': 0.,
        'max_lim': 100.,
    },
    'H': {
        'min_axis': -100.,
        'max_axis': 500.,
        'min_lim': -500.,
        'max_lim': 1000.,
    },
    'LE': {
        'min_axis': -100.,
        'max_axis': 500.,
        'min_lim': -100.,
        'max_lim': 1000.,
    },
    'co2_flux': {
        'min_axis': -50.,
        'max_axis': 50.,
        'min_lim': -200.,
        'max_lim': 200.,
    },
    'u_star': {
        'min_axis': 0.,
        'max_axis': 1.,
        'min_lim': -1.,
        'max_lim': 10.,
    },
    'z_minus_d_over_L': {
        'min_axis': -5.,
        'max_axis': 5.,
        'min_lim': -100.,
        'max_lim': 100.,
    },
    'TKE': {
        'min_axis': 0.,
        'max_axis': 3.,
        'min_lim': -1.,
        'max_lim': 10.,
    },
    'bowen_ratio': {
        'min_axis': -5.,
        'max_axis': 5.,
        'min_lim': -20.,
        'max_lim': 20.,
    },
    'u': {
        'min_axis': -15.,
        'max_axis': 15.,
        'min_lim': -30.,
        'max_lim': 30,
    },
    'v': {
        'min_axis': -15.,
        'max_axis': 15.,
        'min_lim': -30.,
        'max_lim': 30,
    },
    'w': {
        'min_axis': -15.,
        'max_axis': 15.,
        'min_lim': -10.,
        'max_lim': 10.,
    },
    'tau': {
        'min_axis': 0.,
        'max_axis': 2.,
        'min_lim': -1.,
        'max_lim': 10.,
    },
    'h2o_timelag': {
        'min_axis': -2.,
        'max_axis': 2.,
        'min_lim': -10.,
        'max_lim': 10.,
    },
    'co2_timelag': {
        'min_axis': -2.,
        'max_axis': 2.,
        'min_lim': -10.,
        'max_lim': 10.,
    },
}

# PLOT configuration
# ----------------------------------------------------------------------------

# coefficient for figure and text size
COEFF = 2.
# show hour on x-axis
DATE_FORMATTER = mdates.DateFormatter('%H')
# show x-axis tick every 3 hours
DATE_LOCATOR = mdates.HourLocator(byhour=range(0, 25, 3))
# title complement format
TITLE_SUFF_FMT = ' [{:2.0f} min, {:2.0f} m]'
TITLE_2_FMT = '{:%Y-%m-%d}'
TITLE_3_FMT = '{} [{:8.5f}°N, {:8.5f}°E, {:3.0f}m]'


def local_argparser():
    """Parse input arguments and return input arguments as a dictionnary."""
    # argparser default arguments (logs)
    parser = ag.init_argparser(PROG_DESCR, BASE_DIR, PROG_NAME, __version__)

    return parser


def parse_args(input_args):
    """Parse inputs arguments and return dict."""
    # init args
    parser = local_argparser()

    # parse args
    try:
        args = parser.parse_args(input_args)
    except argparse.ArgumentError as exc:
        print('\n', exc)
        sys.exit(1)

    return vars(args)


def read_data(list_files, date):
    """Read data from netCDF file."""
    # data dict
    data = {}

    # date filter
    date_end = date + dt.timedelta(days=1)

    # open files
    print('files to read {}'.format(list_files))
    nc_ids = [nc.Dataset(file_) for file_ in list_files]

    # time variables
    for var in TIME_VARS:
        tmp = [
            nc.num2date(nc_id.variables[var][:],
                        units=nc_ids[0].variables[var].units) for nc_id in nc_ids]
        data[var] = np.concatenate(tmp)

    # create filter for data
    time_filter = (data['time'] >= date) & (data['time'] < date_end)

    # scalar data
    for var in SCALAR_VARS:
        data[var] = nc_ids[0].variables[var][:]

    # vector data
    for var in VECTOR_VARS:
        tmp = [nc_id.variables[var][:] for nc_id in nc_ids]
        data[var] = np.concatenate(tmp)[time_filter]

    # close files
    for nc_id in nc_ids:
        nc_id.close()

    return data


def create_plot_filename(file_, out_dir, desc):
    """Change name of the file to add plot descriptor."""
    # split file name
    file_ = os.path.basename(file_)
    base = os.path.splitext(file_)[0]

    out_file = '{}_{}.png'.format(base, desc)
    out_file = os.path.join(out_dir, out_file)

    return out_file


def increment_color(axis, number):
    """Increments color of line cycler for `axis` axis `number` times."""
    for n in range(number):
        next(axis._get_lines.prop_cycler)

    return axis


def configure_xaxis(axis, metadata):
    """Configure X-axis of the plot."""
    axis.set_xlim(metadata['date'], metadata['date_end'])
    axis.xaxis.set_major_formatter(DATE_FORMATTER)
    axis.xaxis.set_major_locator(DATE_LOCATOR)
    axis.set_xlabel('time $[UTC]$')


def add_titles(title, metadata):
    """Add the 3 levels of titles to the plot."""
    # values
    time_res = metadata['time_res'] / 60
    sensor_alt = metadata['sensor_alt']
    station = metadata['station']
    lat = metadata['lat']
    lon = metadata['lon']
    alt = metadata['alt']

    title_1 = title + TITLE_SUFF_FMT.format(time_res, sensor_alt)
    title_2 = TITLE_2_FMT.format(metadata['date'])
    title_3 = TITLE_3_FMT.format(station, lat, lon, alt)

    plot.title1(title_1, COEFF)
    plot.title2(title_2, COEFF)
    plot.title3(title_3, COEFF)


def check_ylim(data, var_names, axis):
    """Correct y_lim values if any outlier data is detected."""
    # loop over the variables
    y_min, y_max = axis.get_ylim()
    for var in var_names:

        if y_min < PLOT_THRESHOLD[var]['min_lim']:
            y_min = PLOT_THRESHOLD[var]['min_axis']

        if y_max > PLOT_THRESHOLD[var]['max_lim']:
            y_max = PLOT_THRESHOLD[var]['max_axis']

    axis.set_ylim(y_min, y_max)


def plot_ws_wd(data, metadata, file_):
    """Plot ws and wd."""
    title = 'wind speed and direction'

    # figure and axes
    # ------------------------------------------------------------------------
    fig, ax_l = plt.subplots(1)
    ax_r = ax_l.twinx()

    # plot
    # ------------------------------------------------------------------------
    ax_l.plot(data['time'], data['ws'],
              label='wind speed')
    ax_r = increment_color(ax_r, 1)
    ax_r.plot(data['time'], data['wd'],
              linestyle='None', marker='o', markersize=4, alpha=0.5,
              label='wind direction')

    # configure axis
    # ------------------------------------------------------------------------

    # x-axis
    configure_xaxis(ax_l, metadata)

    # left y-axis
    check_ylim(data, ['ws'], ax_l)
    ax_l.set_ylabel('wind speed $[m.s^{-1}]$')

    # right y-axis
    ax_r.set_ylim(0, 360)
    ax_r.yaxis.set_ticks(np.arange(0, 405, 45))
    ax_r.set_ylabel('wind direction $[°]$')

    # grid
    ax_l.grid()

    # legend
    # ------------------------------------------------------------------------
    handle, line = ax_l.get_legend_handles_labels()
    handle_r, line_r = ax_r.get_legend_handles_labels()
    ax_l.legend(handle + handle_r, line + line_r,
                loc=2, bbox_to_anchor=(-0.18, 1.15))

    # titles
    # ------------------------------------------------------------------------
    add_titles(title, metadata)

    # add logo and save
    # ------------------------------------------------------------------------
    plot.add_sirta_logo()
    fig.savefig(file_, dpi=200)


def plot_air_temp(data, metadata, file_):
    """Plot air temp, dew point temp and sonic temp."""
    title = 'Temperature'

    # figure and axes
    # ------------------------------------------------------------------------
    fig, ax_l = plt.subplots(1)

    # plot
    # ------------------------------------------------------------------------
    ax_l.plot(data['time'], data['air_temp'] - DEG_2_K,
              label='air temp')
    ax_l.plot(data['time'], data['dew_temp'] - DEG_2_K,
              label='dew point temp')
    ax_l.plot(data['time'], data['virt_temp'] - DEG_2_K,
              label='sonic temp')

    # configure axis
    # ------------------------------------------------------------------------

    # x-axis
    configure_xaxis(ax_l, metadata)

    # left y-axis
    check_ylim(data, ['air_temp', 'dew_temp', 'virt_temp'], ax_l)
    ax_l.set_ylabel(r'temperature $[^{\circ}C]$')

    # grid
    ax_l.grid()

    # legend
    # ------------------------------------------------------------------------
    ax_l.legend(loc=2, bbox_to_anchor=(-0.15, 1.20))

    # titles
    # ------------------------------------------------------------------------
    add_titles(title, metadata)

    # add logo and save
    # ------------------------------------------------------------------------
    plot.add_sirta_logo()
    fig.savefig(file_, dpi=200)


def plot_mixing_ratio(data, metadata, file_):
    """Plot air temp, dew point temp and sonic temp."""
    title = '$CO_{2}$ and $H_{2}O$ mixing ratio'

    # figures and axes
    # ------------------------------------------------------------------------
    fig, ax_l = plt.subplots(1)
    ax_r = ax_l.twinx()

    # plot
    # ------------------------------------------------------------------------
    ax_l.plot(data['time'], data['h2o_mixing_ratio'],
              label='$H_{2}O$')
    ax_r = increment_color(ax_r, 1)
    ax_r.plot(data['time'], data['co2_mixing_ratio'],
              label='$CO_{2}$')

    # configure axis
    # ------------------------------------------------------------------------

    # x-axis
    configure_xaxis(ax_l, metadata)

    # left y-axis
    check_ylim(data, ['h2o_mixing_ratio'], ax_l)
    ax_l.set_ylabel(r'$H_{2}O$ mixing ratio $[mmol.mol^{-1}]$')

    # right y-axis
    check_ylim(data, ['co2_mixing_ratio'], ax_r)
    ax_r.set_ylabel(r'$CO_{2}$ mixing ratio $[{\mu}mol.mol^{-1}]$')

    # grid
    ax_l.grid()

    # legend
    # ------------------------------------------------------------------------
    handle, line = ax_l.get_legend_handles_labels()
    handle_r, line_r = ax_r.get_legend_handles_labels()
    ax_l.legend(handle + handle_r, line + line_r,
                loc=2, bbox_to_anchor=(-0.15, 1.20))

    # titles
    # ------------------------------------------------------------------------
    add_titles(title, metadata)

    # add logo and save
    # ------------------------------------------------------------------------
    plot.add_sirta_logo()
    fig.savefig(file_, dpi=200)


def plot_concentration(data, metadata, file_):
    """Plot air temp, dew point temp and sonic temp."""
    title = '$CO_{2}$ and $H_{2}O$ molar density'

    # figure and axes
    # ------------------------------------------------------------------------
    fig, ax_l = plt.subplots(1)
    ax_r = ax_l.twinx()

    # plot
    # ------------------------------------------------------------------------
    ax_l.plot(data['time'], data['h2o_molar_density'] * 1.e3,
              label='$H_{2}O$')
    ax_r = increment_color(ax_r, 1)
    ax_r.plot(data['time'], data['co2_molar_density'] * 1.e6,
              label='$CO_{2}$')

    # configure axis
    # ------------------------------------------------------------------------

    # x-axis
    configure_xaxis(ax_l, metadata)

    # left y-axis
    check_ylim(data, ['h2o_molar_density'], ax_l)
    ax_l.set_ylabel('$H_{2}O$ molar density $[mmol.m^{-3}]$')

    # right y-axis
    check_ylim(data, ['co2_molar_density'], ax_r)
    ax_r.set_ylabel('$CO_{2}$ molar density $[µmol.m^{-3}]$')

    # grid
    ax_l.grid()

    # legend
    # ------------------------------------------------------------------------
    handle, line = ax_l.get_legend_handles_labels()
    handle_r, line_r = ax_r.get_legend_handles_labels()
    ax_l.legend(handle + handle_r, line + line_r, loc=2, bbox_to_anchor=(-0.15, 1.20))

    # titles
    # ------------------------------------------------------------------------
    add_titles(title, metadata)

    # add logo and save
    # ------------------------------------------------------------------------
    plot.add_sirta_logo()
    fig.savefig(file_, dpi=200)


def plot_heatflux(data, metadata, file_):
    """Plot heat and cO2 flux."""
    title = 'heat flux and $CO_2$ flux'

    # figure and axes
    # ------------------------------------------------------------------------
    fig = plt.figure()
    grid = gridspec.GridSpec(6, 1)
    # flag axe
    ax_f = fig.add_subplot(grid[5, :])
    # line axe
    ax_l = fig.add_subplot(grid[:5, :], sharex=ax_f)
    ax_r = ax_l.twinx()

    # plot lines
    # ------------------------------------------------------------------------
    ax_l.plot(data['time'], data['H'],
              linestyle='-',
              label='sensible')
    ax_l.plot(data['time'], data['LE'],
              linestyle='-',
              label='latent')
    ax_r = increment_color(ax_r, 2)
    ax_r.plot(data['time'], data['co2_flux'] * 1.e6,
              linestyle='-',
              label='$CO_{2}$')

    # plot quality bars
    # ------------------------------------------------------------------------
    bar_with = metadata['time_res'] / SECONDS_PER_DAY
    # plot H qc
    flag = data['H_qc'] == 2
    ax_f.bar(data['time'][flag], np.ones(data['time'][flag].size), bar_with,
             color='red')
    flag = data['H_qc'] == 1
    ax_f.bar(data['time'][flag], np.ones(data['time'][flag].size), bar_with,
             color='orange')
    flag = data['H_qc'] == 0
    ax_f.bar(data['time'][flag], np.ones(data['time'][flag].size), bar_with,
             color='green')

    # plot LE qc
    flag = data['LE_qc'] == 2
    ax_f.bar(data['time'][flag], np.ones(data['time'][flag].size), bar_with,
             color='red', bottom=1)
    flag = data['LE_qc'] == 1
    ax_f.bar(data['time'][flag], np.ones(data['time'][flag].size), bar_with,
             color='orange', bottom=1)
    flag = data['LE_qc'] == 0
    ax_f.bar(data['time'][flag], np.ones(data['time'][flag].size), bar_with,
             color='green', bottom=1)

    # plot CO2 qc
    flag = data['co2_flux_qc'] == 2
    ax_f.bar(data['time'][flag], np.ones(data['time'][flag].size), bar_with,
             color='red', bottom=2)
    flag = data['co2_flux_qc'] == 1
    ax_f.bar(data['time'][flag], np.ones(data['time'][flag].size), bar_with,
             color='orange', bottom=2)
    flag = data['co2_flux_qc'] == 0
    ax_f.bar(data['time'][flag], np.ones(data['time'][flag].size), bar_with,
             color='green', bottom=2)

    # configure axis
    # ------------------------------------------------------------------------

    # master x-axis (flag)
    configure_xaxis(ax_f, metadata)

    # slave x-axis (line) hide ticks
    plt.setp(ax_l.get_xmajorticklabels(), visible=False)

    # line left y-axis
    check_ylim(data, ['H', 'LE'], ax_l)
    ax_l.set_ylabel('heat flux $[W.m^{-2}]$')

    # line right y-axis
    check_ylim(data, ['co2_flux'], ax_r)
    ax_r.set_ylabel('$CO_2$ flux $[µmol.m^{-2}.s^{-1}]$')

    # line grid
    ax_l.grid()

    # flag y-axis
    ax_f.set_ylim(0, 3)
    ax_f.yaxis.set_ticks(np.array([0.5, 1.5, 2.5]))
    ax_f.set_yticklabels(['sensible', 'latent', '$CO_{2}$'])
    for tick in ax_f.yaxis.get_major_ticks():
        tick.label.set_fontsize(6)

    # legend
    # ------------------------------------------------------------------------

    # line plot
    handle, line = ax_l.get_legend_handles_labels()
    handle_r, line_r = ax_r.get_legend_handles_labels()
    ax_l.legend(handle + handle_r, line + line_r,
                loc=2, bbox_to_anchor=(-0.15, 1.25))

    # flag plot
    patches = [
        mpatches.Patch(color='green', label='good'),
        mpatches.Patch(color='orange', label='ok'),
        mpatches.Patch(color='red', label='bad'),
    ]
    ax_f.legend(handles=patches, loc=1, bbox_to_anchor=(1.16, 1.17))

    # titles
    # ------------------------------------------------------------------------
    add_titles(title, metadata)

    # add logo and save
    # ------------------------------------------------------------------------
    plot.add_sirta_logo()
    fig.savefig(file_, dpi=200)


def plot_ustar(data, metadata, file_):
    """Plot air temp, dew point temp and sonic temp."""
    title = r'$u^{*}$ and $\frac{z-d}{L}$'

    # figure and axes
    fig, ax_l = plt.subplots(1)
    ax_r = ax_l.twinx()

    # plot
    # ------------------------------------------------------------------------
    ax_l.plot(data['time'], data['u_star'],
              label='$u^{*}$')
    ax_r = increment_color(ax_r, 1)
    ax_r.plot(data['time'], data['z_minus_d_over_L'],
              linestyle='None', marker='o', markersize=4, alpha=0.5,
              label=r'$\frac{z-d}{L}$')

    # configure axis
    # ------------------------------------------------------------------------

    # x-axis
    configure_xaxis(ax_l, metadata)

    # left y-axis
    ax_l.set_ylabel('$u^{*}$ $[m.s^{-1}]$')
    check_ylim(data, ['u_star'], ax_l)

    # right y-axis
    check_ylim(data, ['z_minus_d_over_L'], ax_r)
    ax_r.set_ylabel(r'$\frac{z-d}{L}$ $[unitless]$')

    # grid
    ax_l.grid()

    # legend
    # ------------------------------------------------------------------------
    handle, line = ax_l.get_legend_handles_labels()
    handle_r, line_r = ax_r.get_legend_handles_labels()
    ax_l.legend(handle + handle_r, line + line_r, loc=2,
                bbox_to_anchor=(-0.15, 1.20))

    # titles
    # ------------------------------------------------------------------------
    add_titles(title, metadata)

    # add logo and save
    # ------------------------------------------------------------------------
    plot.add_sirta_logo()
    fig.savefig(file_, dpi=200)


def plot_tke(data, metadata, file_):
    """Plot Turbulent Kinetic Energy."""
    title = 'Turbulent Kinetic Energy'

    # figure and axes
    # ------------------------------------------------------------------------
    fig, ax_l = plt.subplots(1)

    # plot
    # ------------------------------------------------------------------------
    ax_l.plot(data['time'], data['TKE'],
              label='TKE')

    # configure axis
    # ------------------------------------------------------------------------

    # x-axis
    configure_xaxis(ax_l, metadata)

    # y-axis
    check_ylim(data, ['TKE'], ax_l)
    ax_l.set_ylabel('TKE $[m^{2}.s^{-1}]$')

    # grid
    ax_l.grid()

    # titles
    # ------------------------------------------------------------------------
    add_titles(title, metadata)

    # add logo and save
    # ------------------------------------------------------------------------
    plot.add_sirta_logo()
    fig.savefig(file_, dpi=200)


def plot_bowen(data, metadata, file_):
    """Plot bowen ratio."""
    title = 'Bowen Ratio'

    # figure and axes
    # ------------------------------------------------------------------------
    fig, ax_l = plt.subplots(1)

    # plot
    # ------------------------------------------------------------------------
    ax_l.plot(data['time'], data['bowen_ratio'],
              label='bowen ratio')

    # configure axis
    # ------------------------------------------------------------------------

    # x-axis
    configure_xaxis(ax_l, metadata)

    # y-axis
    check_ylim(data, ['bowen_ratio'], ax_l)
    ax_l.set_ylabel('bowen ratio $[unitless]$')

    # grid
    ax_l.grid()

    # titles
    # ------------------------------------------------------------------------
    add_titles(title, metadata)

    # add logo and save
    # ------------------------------------------------------------------------
    plot.add_sirta_logo()
    fig.savefig(file_, dpi=200)


def plot_u_v_w(data, metadata, file_):
    """Plot u, v, w and spikes."""
    title = 'wind components'

    # figure and axes
    # ------------------------------------------------------------------------
    fig, ax_l = plt.subplots(2, sharex=True)

    # plot
    # ------------------------------------------------------------------------

    # lines
    line_u, = ax_l[0].plot(data['time'], data['u'],
                           label='u')
    ax_l[0].fill_between(data['time'],
                         data['u'] + data['u_var'],
                         data['u'] - data['u_var'],
                         color=line_u.get_color(), alpha=0.3)
    line_v, = ax_l[0].plot(data['time'], data['v'],
                           label='v')
    ax_l[0].fill_between(data['time'],
                         data['v'] + data['v_var'],
                         data['v'] - data['v_var'],
                         color=line_v.get_color(), alpha=0.3)
    line_w, = ax_l[0].plot(data['time'], data['w'],
                           label='w')
    ax_l[0].fill_between(data['time'],
                         data['w'] + data['w_var'],
                         data['w'] - data['w_var'],
                         color=line_w.get_color(), alpha=0.3)

    # bars
    bar_with = metadata['time_res'] / 3 / SECONDS_PER_DAY
    bar_loc_offset = dt.timedelta(seconds=metadata['time_res']/3)
    ax_l[1].bar(data['time'],
                data['u_spikes'],
                bar_with,
                label='u spikes')
    ax_l[1].bar([d + bar_loc_offset for d in data['time']],
                data['v_spikes'],
                bar_with,
                label='v spikes')
    ax_l[1].bar([d + 2 * bar_loc_offset for d in data['time']],
                data['w_spikes'],
                bar_with,
                label='w spikes')

    # configure axis
    # ------------------------------------------------------------------------

    # master x-axis
    configure_xaxis(ax_l[1], metadata)

    # y-axis
    check_ylim(data, ['u', 'v', 'w'], ax_l[0])
    ax_l[0].set_ylabel('Wind speed\n$[m.s^{-1}]$')
    ax_l[1].set_ylabel('Number of spikes\n$[unitless]$')

    # grid
    ax_l[0].grid()
    ax_l[1].grid()

    # legend
    # ------------------------------------------------------------------------
    ax_l[0].legend(loc=2, bbox_to_anchor=(-0.15, 1.45))

    # titles
    # ------------------------------------------------------------------------
    add_titles(title, metadata)

    # add logo and save
    # ------------------------------------------------------------------------
    plot.add_sirta_logo()
    fig.savefig(file_, dpi=200)


def plot_h2o_co2_spikes(data, metadata, file_):
    """Plot H2O, CO2 spikes."""
    title = '$CO_{2}$ et $H_{2}O$ Number of spikes'

    # figure and axes
    # ------------------------------------------------------------------------
    fig, ax_l = plt.subplots(2, sharex=True)
    ax_r = ax_l[0].twinx()

    # plot
    # ------------------------------------------------------------------------

    # line
    ax_l[0].plot(data['time'], data['h2o_mixing_ratio'],
                 label='$H_{2}O$')
    ax_r = increment_color(ax_r, 1)
    ax_r.plot(data['time'], data['co2_mixing_ratio'],
              label='$CO_{2}$')

    # bars
    # convert to float and replace missing values by NaN
    h2o_spikes = data['h2o_spikes'].astype('f4')
    missing_filter = h2o_spikes == -9
    h2o_spikes[missing_filter] = np.nan

    co2_spikes = data['co2_spikes'].astype('f4')
    missing_filter = co2_spikes == -9
    co2_spikes[missing_filter] = np.nan

    bar_with = metadata['time_res'] / 2 / SECONDS_PER_DAY
    bar_loc_offset = dt.timedelta(seconds=metadata['time_res']/2)
    ax_l[1].bar(data['time'],
                h2o_spikes,
                bar_with,
                label='$H_{2}O$')
    ax_l[1].bar([d + bar_loc_offset for d in data['time']],
                co2_spikes,
                bar_with,
                label='$CO_{2}$')

    # configure axis
    # ------------------------------------------------------------------------

    # master x-axis (bar)
    configure_xaxis(ax_l[1], metadata)

    # left y-axis (line)
    ax_l[0].set_ylabel('$H_{2}O$ mixing ratio\n' + r'$[mmol.mol^{-1}]$')

    # right y-axis (line)
    ax_r.set_ylabel('$CO_{2}$ mixing ratio\n' + r'$[{\mu}mol.mol^{-1}]$')

    # left y-axis (bars)
    ax_l[1].set_ylabel('Number of spikes\n$[unitless]$')

    # grid
    ax_l[0].grid()
    ax_l[1].grid()

    # legend
    # ------------------------------------------------------------------------
    handle, line = ax_l[0].get_legend_handles_labels()
    handle_r, line_r = ax_r.get_legend_handles_labels()
    ax_l[0].legend(handle + handle_r, line + line_r,
                   loc=2, bbox_to_anchor=(-0.15, 1.40))

    # titles
    # ------------------------------------------------------------------------
    add_titles(title, metadata)

    # add logo and save
    # ------------------------------------------------------------------------
    plot.add_sirta_logo()
    fig.savefig(file_, dpi=200)


def plot_ts(data, metadata, file_):
    """Plot virtual temperature and spikes."""
    title = 'Sonic temperature'

    # figure and axes
    # ------------------------------------------------------------------------
    fig, ax_l = plt.subplots(2, sharex=True)

    # plot
    # ------------------------------------------------------------------------

    # line
    line_ts, = ax_l[0].plot(data['time'], data['virt_temp']- DEG_2_K,
                            label='sonic temperature')
    ax_l[0].fill_between(data['time'],
                         data['virt_temp']- DEG_2_K + data['u_var'],
                         data['virt_temp']- DEG_2_K - data['u_var'],
                         color=line_ts.get_color(), alpha=0.3)

    # bar
    bar_with = metadata['time_res'] / SECONDS_PER_DAY
    ax_l[1].bar(data['time'], data['ts_spikes'], bar_with,
                label='sonic temperature spikes')

    # configure axis
    # ------------------------------------------------------------------------

    # master x-axis
    configure_xaxis(ax_l[1], metadata)

    # y-axis (lines)
    check_ylim(data, ['virt_temp'], ax_l[0])
    ax_l[0].set_ylabel('Temperature\n$[°C]$')

    # y-axis (bars)
    ax_l[1].set_ylabel('Number of spikes\n$[unitless]$')

    # grid
    ax_l[0].grid()
    ax_l[1].grid()

    # titles
    # ------------------------------------------------------------------------
    add_titles(title, metadata)

    # add logo and save
    # ------------------------------------------------------------------------
    plot.add_sirta_logo()
    fig.savefig(file_, dpi=200)


def plot_tau(data, metadata, file_):
    """Plot tau."""
    title = 'Tau'

    # figure ans axes
    # ------------------------------------------------------------------------
    fig = plt.figure()
    grid = gridspec.GridSpec(9, 1)
    ax_f = fig.add_subplot(grid[8, :])
    ax_l = fig.add_subplot(grid[:8, :], sharex=ax_f)

    # plot
    # ------------------------------------------------------------------------

    # line
    ax_l.plot(data['time'], data['tau'],
              label='tau')

    # flag
    bar_with = metadata['time_res'] / SECONDS_PER_DAY
    flag = data['tau_qc'] == 2
    ax_f.bar(data['time'][flag], np.ones(data['time'][flag].size), bar_with, color='red')
    flag = data['tau_qc'] == 1
    ax_f.bar(data['time'][flag], np.ones(data['time'][flag].size), bar_with, color='orange')
    flag = data['tau_qc'] == 0
    ax_f.bar(data['time'][flag], np.ones(data['time'][flag].size), bar_with, color='green')

    # configure axis
    # ------------------------------------------------------------------------

    # master x-axis (flag)
    configure_xaxis(ax_f, metadata)

    # slave x-axis (line)
    plt.setp(ax_l.get_xmajorticklabels(), visible=False)

    # y-axis (line)
    check_ylim(data, ['tau'], ax_l)
    ax_l.set_ylabel('tau $[kg.m^{-1}.s^{-2}]$')

    # y-axis (flag)
    ax_f.set_ylim(0, 1)
    ax_f.set_yticklabels(['Tau'])
    ax_f.yaxis.set_ticks(np.array([0.5]))
    for tick in ax_f.yaxis.get_major_ticks():
        tick.label.set_fontsize(6)

    # grid
    ax_l.grid()


    # legend
    # ------------------------------------------------------------------------
    patches = [
        mpatches.Patch(color='green', label='good'),
        mpatches.Patch(color='orange', label='ok'),
        mpatches.Patch(color='red', label='bad')
    ]
    ax_f.legend(handles=patches, loc=1, bbox_to_anchor=(1.16, 1.50))

    # titles
    # ------------------------------------------------------------------------
    add_titles(title, metadata)

    # add logo and save
    # ------------------------------------------------------------------------
    plot.add_sirta_logo()
    fig.savefig(file_, dpi=200)


def plot_n_record(data, metadata, file_):
    """Plot number of record used."""
    title = 'Used records'

    # figure and axes
    # ------------------------------------------------------------------------
    fig, ax_l = plt.subplots(1)

    # plot
    # ------------------------------------------------------------------------
    bar_with = metadata['time_res'] / SECONDS_PER_DAY
    ax_l.bar(data['time'], data['used_record'], bar_with,
             label='used records')

    # configure axis
    # ------------------------------------------------------------------------

    # x-axis
    configure_xaxis(ax_l, metadata)

    # y-axis
    ax_l.set_ylabel('used record $[unitless]$')

    # grid
    ax_l.grid()

    # titles
    # ------------------------------------------------------------------------
    add_titles(title, metadata)

    # add logo and save
    # ------------------------------------------------------------------------
    plot.add_sirta_logo()
    fig.savefig(file_, dpi=200)


def plot_timelag(data, metadata, file_):
    """Plot timelag."""
    title = 'Time lags'

    # figure and axes
    # ------------------------------------------------------------------------
    fig, ax_l = plt.subplots(1)

    # plot
    # ------------------------------------------------------------------------
    bar_with = metadata['time_res'] / 2 / SECONDS_PER_DAY
    bar_loc_offset = dt.timedelta(seconds=metadata['time_res']/2)
    ax_l.bar(data['time'],
             data['h2o_timelag'],
             bar_with,
             label='$w-H_{2}O$')
    ax_l.bar([d + bar_loc_offset for d in data['time']],
             data['co2_timelag'],
             bar_with,
             label='$w-CO_{2}$')
    ax_l.axhline(0, color='k')

    # configure axis
    # ------------------------------------------------------------------------

    # x-axis
    configure_xaxis(ax_l, metadata)

    # y-axis
    check_ylim(data, ['h2o_timelag', 'co2_timelag'], ax_l)
    ax_l.set_ylabel('lag $[seconds]$')

    # grid
    ax_l.grid()

    # legend
    # ------------------------------------------------------------------------
    ax_l.legend(loc=2, bbox_to_anchor=(-0.15, 1.20))

    # titles
    # ------------------------------------------------------------------------
    add_titles(title, metadata)

    # add logo and save
    # ------------------------------------------------------------------------
    plot.add_sirta_logo()
    fig.savefig(file_, dpi=200)


def main(raw_args):
    """Process data."""
    # init script
    # ------------------------------------------------------------------------

    # parse inputs arguments
    args = parse_args(raw_args)

    # welcome message
    utils.welcome(PROG_NAME, __version__)

    # debug show input arguments
    print('date to process {:%Y-%m-%d}'.format(args['date']))

    date = args['date']
    list_files = args['input_file'][0]
    out_dir = args['output_dir']

    # read data
    # ------------------------------------------------------------------------
    data = read_data(list_files, date)

    # determine common metadata for plots
    metadata = {
        'date' : date,
        'date_end': date + ALMOST_ONE_DAY,
        'time_res': (data['time'][1] - data['time'][0]).total_seconds(),
        'station': str(nc.chartostring(data['station_name'])),
        'lat': float(data['station_lat']),
        'lon': float(data['station_lon']),
        'sensor_alt': float(data['sensor_alt_agl']),
        'alt': float(data['sensor_alt']) - float(data['sensor_alt_agl']),
    }

    # create plot
    # ------------------------------------------------------------------------

    # wind speed and direction
    plot_ws_wd(data, metadata, os.path.join(out_dir, 'plot_1.png'))

    # temperature
    plot_air_temp(data, metadata, os.path.join(out_dir, 'plot_2.png'))

    # mixing ratio
    if data['instrument_type'] == 0:
        plot_mixing_ratio(data, metadata, os.path.join(out_dir, 'plot_3.png'))

    # concentration
    if data['instrument_type'] == 0:
        plot_concentration(data, metadata, os.path.join(out_dir, 'plot_4.png'))

    # heatflux
    plot_heatflux(data, metadata, os.path.join(out_dir, 'plot_5.png'))

    # u_star and Z/L
    plot_ustar(data, metadata, os.path.join(out_dir, 'plot_6.png'))

    # TKE
    plot_tke(data, metadata, os.path.join(out_dir, 'plot_7.png'))

    # bowen ratio
    if data['instrument_type'] == 0:
        plot_bowen(data, metadata, os.path.join(out_dir, 'plot_8.png'))

    # u, v, w and spikes
    plot_u_v_w(data, metadata, os.path.join(out_dir, 'plot_a.png'))

    # h2o, co2 and spikes
    if data['instrument_type'] == 0:
        plot_h2o_co2_spikes(data, metadata, os.path.join(out_dir, 'plot_b.png'))

    # TS spikes
    plot_ts(data, metadata, os.path.join(out_dir, 'plot_d.png'))

    # tau
    plot_tau(data, metadata, os.path.join(out_dir, 'plot_e.png'))

    # used record
    plot_n_record(data, metadata, os.path.join(out_dir, 'plot_f.png'))

    # timelag
    if data['instrument_type'] == 0:
        plot_timelag(data, metadata, os.path.join(out_dir, 'plot_g.png'))

    return 0


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
