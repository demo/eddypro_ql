| branch  | status  |
| --- | --- |
| Master | [![build status](https://git.icare.univ-lille1.fr/demo/eddypro_ql/badges/master/build.svg)](https://git.icare.univ-lille1.fr/demo/eddypro_ql/commits/master) [![coverage report](https://git.icare.univ-lille1.fr/demo/eddypro_ql/badges/master/coverage.svg)](https://git.icare.univ-lille1.fr/demo/eddypro_ql/commits/master) |
| Development | [![build status](https://git.icare.univ-lille1.fr/demo/eddypro_ql/badges/develop/build.svg)](https://git.icare.univ-lille1.fr/demo/eddypro_ql/commits/develop) [![coverage report](https://git.icare.univ-lille1.fr/demo/eddypro_ql/badges/develop/coverage.svg)](https://git.icare.univ-lille1.fr/demo/eddypro_ql/commits/develop) |

# Eddypro 2a Quicklooks

Ce code créé des quicklooks avec un logo AERIS à partir des fichiers 2a netCDF Eddypro

auteur: marc-antoine drouin (marc-antoine.drouin@lmd.polytechnique.fr)

## Pré-requis

Ce code fonctionne uniquement avec une version de python >=3.5

modules nécessaires pour exécuter le code:
numpy >= 1.13.0
netCDF4 >= 1.2.9
matplotlib >= 2.0.0

pour exécuter les tests:
pytest

## Tester le code:

se placer à la racine de ce dossier et exécuter la commande:

```bash
py.test
```

## Utilisation

```bash
python yyyymmdd fichier_netcdf.nc dossier_sortie
```

## Fichiers de sortie

Ce code va créer des image nommées `plot_i.png` où `i` est un chiffre pour les quicklooks publics et une lettre pour les quicklooks PI.
