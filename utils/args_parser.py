#!/usr/bin/env python
# -*- coding: utf8 -*-

"""module for arguments managements"""

from __future__ import print_function
from __future__ import division
from __future__ import absolute_import

import sys
import os
import importlib
import glob
import argparse
import datetime as dt
from itertools import chain

from . import logs
from . import utils


DATE_FMT = '%Y%m%d'
DATE_FMT_HOURLY = '%Y%m%d:%H'


def check_date_format(input_date):
    """Check the format of the date argument."""
    try:
        dt_date = dt.datetime.strptime(input_date, DATE_FMT)
    except ValueError:
        try:
            dt_date = dt.datetime.strptime(input_date, DATE_FMT_HOURLY)
        except ValueError:
            msg = '{} has not the required format (YYYYMMDD or YYYYMMDD:HH)'.format(input_date)
            raise argparse.ArgumentTypeError(msg)

    return dt_date


def check_input_files(input_files):
    """Check if the input files exist and return a list of the input files found."""
    list_files = glob.glob(input_files)

    if not list_files:
        msg = "No input files found corresponding to the file pattern "
        msg += input_files
        raise argparse.ArgumentTypeError(msg)
    elif len(list_files) == 1:
        return list_files

    list_ = sorted(list_files)
    list_ = [f for f in chain.from_iterable(list_)]

    return list_


def read_list_files(file_):
    """Load contains of file containing a list of files and returns it as a list."""
    print('# read_list_files')
    print(file_)

    with open(file_, 'r') as f_id:
        files = [line.strip() for line in f_id.readlines()]

    print(files)

    return files


def check_input_lists_files(input_files):
    """Check input files lists and return a list of each input."""
    # check if files exist
    input_list = check_input_files(input_files)

    # build list of list of input files
    inputs_lists = []
    for file_ in input_list:
        # read the list of files
        list_files = read_list_files(file_)
        # check if files can be found
        for file_data in list_files:
            # in case there is no first file
            if file_data == '':
                tmp = [file_data]
            else:
                tmp = check_input_files(file_data)
            inputs_lists.append(tmp[0])

    return inputs_lists


def check_output_dir(output_dir):
    """Check if the directory provided for the output file is writable."""
    if not utils.check_dir(output_dir):
        msg = "output directory " + output_dir
        msg += " doesn't exist or is not writable"
        raise argparse.ArgumentTypeError(msg)

    return output_dir


def check_conf_file(input_conf):
    """Check if conf file can be loaded and load it if possible."""
    list_files = glob.glob(input_conf)

    if len(list_files) != 1:
        msg = "error: Configuration file {} cannot be found"
        raise argparse.ArgumentError('conf', msg.format(input_conf))

    # extract path
    file_ = os.path.abspath(list_files[0])
    module_path = os.path.dirname(file_)
    module_name = os.path.splitext(os.path.basename(file_))[0]

    # check if module path is in python path. if not add it
    if module_path not in sys.path:
        sys.path.append(module_path)

    try:
        conf = importlib.import_module(module_name)
    except ImportError:
        msg = 'error: cannot import module {}'
        raise argparse.ArgumentError('conf', msg.format(list_files[0]))

    return conf

def check_output_file(output_list_file):
    """Check the file containing the list exists and check that output directory is writtable."""
    # check if files exist
    output_list_file = check_input_files(output_list_file)[0]

    # read files
    list_files = read_list_files(output_list_file)

    # check if we can write into the directory
    # if not program will quit
    for file_ in list_files:
        check_output_dir(file_)

    return list_files


def init_argparser(descr, prog_dir, prog_name, version):
    """Parse input arguments."""
    parser = argparse.ArgumentParser(description=descr)

    # arguments from the workflow
    parser.add_argument('date',
                        type=check_date_format,
                        help='date to process (yyyymmdd)')
    parser.add_argument('input_file',
                        type=check_input_files,
                        nargs='*')
    parser.add_argument('output_dir',
                        type=check_output_dir,
                        help='output directory')
    parser.add_argument('-v', '--version',
                        action='version',
                        version=version,
                        help='show major version of code')

    return parser
