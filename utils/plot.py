"""
function needed to create plot with SIRTA formatting
"""

import os
from cycler import cycler

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from mpl_toolkits.axes_grid1 import make_axes_locatable


BASE_DIR = os.path.dirname(os.path.dirname(__file__))

COEFF = 2.


COLORS = [
    '#1f77b4', '#ff7f0e', '#2ca02c',
    '#d62728', '#9467bd', '#8c564b',
    '#e377c2', '#7f7f7f', '#bcbd22',
    '#17becf'
]

plt.rcParams['axes.prop_cycle'] = cycler('color', COLORS)

plt.rcParams['xtick.major.pad'] = 1.5 * COEFF
plt.rcParams['xtick.minor.pad'] = 1.5 * COEFF
plt.rcParams['ytick.major.pad'] = 1.5 * COEFF
plt.rcParams['ytick.minor.pad'] = 1.5 * COEFF

plt.rcParams['xtick.major.size'] = 1. * COEFF
plt.rcParams['xtick.minor.size'] = 1. * COEFF
plt.rcParams['ytick.major.size'] = 1. * COEFF
plt.rcParams['ytick.minor.size'] = 1. * COEFF

plt.rcParams['xtick.labelsize'] = 5 * COEFF
plt.rcParams['ytick.labelsize'] = 5 * COEFF

plt.rcParams['axes.linewidth'] = 0.5 * COEFF
plt.rcParams['axes.labelsize'] = 5 * COEFF
plt.rcParams['axes.facecolor'] = '#ffffff'

plt.rcParams['legend.numpoints'] = 1
plt.rcParams['legend.fontsize'] = 3.5 * COEFF
plt.rcParams['legend.facecolor'] = '#ffffff'

plt.rcParams['grid.linestyle'] = ':'
plt.rcParams['grid.linewidth'] = 0.5 *COEFF
plt.rcParams['grid.alpha'] = 0.5


plt.rcParams['figure.subplot.hspace'] = 0.2
plt.rcParams['figure.subplot.wspace'] = 0.2
plt.rcParams['figure.subplot.bottom'] = .11
plt.rcParams['figure.subplot.left'] = .14
plt.rcParams['figure.subplot.right'] = .86
plt.rcParams['figure.subplot.top'] = .82

plt.rcParams['figure.figsize'] = 2.913 * COEFF, 2.047 * COEFF
plt.rcParams['figure.facecolor'] = '#ffffff'

plt.rcParams['lines.markersize'] = 2.6 * COEFF
plt.rcParams['lines.markeredgewidth'] = 0.5 * COEFF
plt.rcParams['lines.linewidth'] = 0.5 * COEFF


def title1(mytitle, coef):
    """Add level 1 title (title of the plot)."""
    plt.figtext(0.5, 0.95, mytitle, fontsize=6.5*coef, fontweight='bold',
                horizontalalignment='center', verticalalignment='center')
    return


def title2(mytitle, coef):
    """Add level 2 title (date)."""
    plt.figtext(0.5, 0.89, mytitle, fontsize=5.5*coef,
                horizontalalignment='center', verticalalignment='center')
    return


def title3(mytitle, coef):
    """Add level 3 title (localization)."""
    plt.figtext(0.5, 0.85, mytitle, fontsize=4.5*coef,
                horizontalalignment='center', verticalalignment='center')
    return


def add_sirta_logo():
    """Add SIRTA logo on the bottom right of the plot."""
    plt.axes([0.9, -0.01, 0.09, 0.09])
    plt.axis('off')

    try:
        sirtalogo = plt.imread(os.path.join(BASE_DIR, 'logo', 'logo_aeris.png'))
        plt.imshow(sirtalogo, origin='upper')
    except IOError:
        print("fichier graphsirta : Impossible d'inclure le logo !!!!!")

    return


def tmp_f(date_dt):
    """Convert datetime to numerical date."""
    return mdates.num2date(date_dt).strftime('%H')


def cb_room(axe, add_to_figure=False, pad=0.2):
    """Create a new axis from an existing one."""
    divider = make_axes_locatable(axe)
    cb_ax = divider.append_axes(
        "right", size="1%", pad=pad, add_to_figure=add_to_figure)

    return cb_ax
